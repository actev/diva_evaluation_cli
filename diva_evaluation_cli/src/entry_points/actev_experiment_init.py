"""Entry point module: experiment-init

Implements the entry-point by using Python or any other languages.
"""


def entry_point(file_index, activity_index, chunks,
                video_location, system_cache_dir, config_file=None,
                prepare_proposal_outputs=False,
                prepare_localization_outputs=False):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Start servers, starts cluster, etc.

    Args:
        file_index(str): Path to file index json file for test set
        activity_index(str): Path to activity index json file for test set
        chunks (str): Path to chunks json file
        video_location (str): Path to videos content
        system_cache_dir (str): Path to system cache directory
        config_file (str, optional): Path to config file
        prepare_proposal_outputs (str, optional): If True, the system will
            retain proposal output information during execution and output
            the proposals during merge_chunks
        prepare_localization_outputs (str, optional): If True, the system will
            retain spatial localization output information during execution and
            output the proposals during merge_chunks

    """
    raise NotImplementedError("You should implement the entry_point method.")
