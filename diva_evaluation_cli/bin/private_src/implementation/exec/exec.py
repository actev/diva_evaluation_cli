"""Exec module: exec method

"""

import json

from diva_evaluation_cli.src.entry_points.actev_design_chunks import \
    entry_point as design_chunks
from diva_evaluation_cli.src.entry_points.actev_experiment_init import \
    entry_point as experiment_init
from diva_evaluation_cli.src.entry_points.actev_experiment_cleanup import \
    entry_point as experiment_cleanup
from diva_evaluation_cli.src.entry_points.actev_pre_process_chunk import \
    entry_point as pre_process_chunk
from diva_evaluation_cli.src.entry_points.actev_process_chunk import \
    entry_point as process_chunk
from diva_evaluation_cli.src.entry_points.actev_post_process_chunk import \
    entry_point as post_process_chunk
from diva_evaluation_cli.src.entry_points.actev_reset_chunk import \
    entry_point as reset_chunk
from diva_evaluation_cli.src.entry_points.actev_merge_chunks import \
    entry_point as merge_chunks
from diva_evaluation_cli.src.entry_points.actev_experiment_cleanup import \
    entry_point as experiment_cleanup


def exec(file_index, activity_index, chunks, nb_videos_per_chunk, video_location,
         system_cache_dir, config, output, chunk_result, proposals,
         localization):
    """
    Args:
        file_index (str): Path to file index json file for test set
        activity_index (str): Path to activity index json file for test set
        chunks (str): Path to chunks json file
        nb_videos_per_chunk (int): Number of videos in the chunk
        video_location (str): Path to videos content
        system_cache_dir (str): Path to system cache directory
        config (str): Path to config file
        output (str): Path to merge chunks command result
        chunk_result (str): Path to chunks json file after merge chunks
            execution
        proposals (str): Path to proposal output
        localization (str): Path to output with localization
    """

    chunks_list = []
    design_chunks(file_index, activity_index, chunks, nb_videos_per_chunk)

    try:
        experiment_init(file_index, activity_index, chunks, video_location,
                        system_cache_dir, config,
                        prepare_proposal_output=bool(proposals),
                        prepare_localization_output=bool(localization))
        get_chunk_ids(chunks, chunks_list)

        for chunk_id in chunks_list:
            try:
                pre_process_chunk(chunk_id, system_cache_dir)
                process_chunk(chunk_id, system_cache_dir)
                post_process_chunk(chunk_id, system_cache_dir)
            except KeyboardInterrupt:
                experiment_cleanup(system_cache_dir)
                raise KeyboardInterrupt
            except Exception:
                reset_chunk(chunk_id, system_cache_dir)
                continue

        merge_chunks(system_cache_dir, output, chunk_result, None,
                     proposals, localization)
        experiment_cleanup(system_cache_dir)

    except KeyboardInterrupt:
        experiment_cleanup(system_cache_dir)
        raise KeyboardInterrupt
    except Exception:
        experiment_cleanup(system_cache_dir)
        raise Exception


def get_chunk_ids(chunk_file, chunks_list):
    """ Get chunk ids from a chunk json file.

    Args:
        chunk_file (str): Path to a json chunk file

    """
    chunks = json.load(open(chunk_file, 'r'))
    for chunk_id in chunks:
        chunks_list.append(chunk_id)
