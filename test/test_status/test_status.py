'''
This module implements unit tests of the actev status command.
'''

import sys
import os
import unittest

from test import test_cli

cdir = os.path.dirname(os.path.abspath(__file__))
res = os.path.join(cdir, "resources")


class TestStatus(unittest.TestCase):
    """
    Test the actev status command.
    """

    def setUp(self):
        sys.argv[1:] = ["status"]

    def test_status_chunk_query(self):
        chunk_id = 0
        sys.argv.extend(["chunk-query", "-i", chunk_id])
        test_cli('`actev status chunk-query` failed.')

    def test_status_experiment_query(self):
        sys.argv.append("experiment-query")
        test_cli('`actev status experiment-query` failed.')

    def test_status_system_query(self):
        sys.argv.append("system-query")
        test_cli('`actev status system-query` failed.')


if __name__ == '__main__':
    unittest.main()
