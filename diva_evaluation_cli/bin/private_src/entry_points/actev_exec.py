"""Entry point module: exec

This file should not be modified.
"""

from diva_evaluation_cli.bin.private_src.implementation.exec.exec import exec


def entry_point(file_index, activity_index, chunks, nb_videos_per_chunk,
                video_location, system_cache_dir, output_file, chunks_result,
                config_file=None, proposals=False,
                localization=False):
    """Private entry point.

    Calls a team-implemented API. Captures time stamps, resource usage, etc.

    Args:
        file_index (str): Path to file index json file for test set
        activity_index (str): Path to activity index json file for test set
        chunks (str): Path to chunks json file
        nb_videos_per_chunk (int): Number of videos in the chunk
        video_location (str): Path to videos content
        system_cache_dir (str): Path to system cache directory
        output_file (str): Path to merge chunks command result
        chunks_result (str): Path to chunks json file after merge chunks
            execution
        config_file (str, optional): Path to config file
    Optional args:
        proposals (Optional(str, bool)): If set, system should produce proposal outputs
        localization (Optional(str, bool)): If set, system should produce localization outputs

    """
    exec(file_index, activity_index, chunks, nb_videos_per_chunk,
         video_location, system_cache_dir, config_file, output_file,
         chunks_result, proposals, localization)
