# actev status

## Description

Executable at any time. Get the status of the experiment.

The command contains the following subcommands:

## status system-query

### Description

Get the status of the system

### Usage

```bash
actev status system-query
```

## status experiment-query

### Description

Get the status of the experiment

### Usage

```bash
actev status experiment-query
```

## status chunk-query

### Description

Get the status of a chunk id

### Parameters

| Name      | Id | Required        | Definition                 |
|-----------|----|-----------------|----------------------------|
| chunk_id       | i  | True            | chunk id      |

### Usage

Generic command:

```bash
actev status chunk-query -i <chunk id>
```

Example:

```bash
actev status chunk-query -i Chunk1
```
