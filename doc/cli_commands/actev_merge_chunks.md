# actev merge-chunks

## Description

Given a list of chunk ids, merges all the chunks system output present in the list.

This command requires the following parameters:

## Parameters

| Name      | Id         | Required | Definition                 |
|-----------|------------|---------|----------------------------|
| system-cache-dir   | s | True    | path to system cache directory   |
| chunk-file         | c | True    | path to generate a chunk file, that summarizes all the processed videos/activities |
| output-file        | o | True    | path to the output file containing merged chunks results                              |
| chunk-ids          | i | True    | list of chunk ids                                              |
| proposals          | p | False   | path to generate a proposals file, that contains the system proposals |
| localization       | l | False   | path to generate a localization file |

## Usage

Generic command:

```bash
actev merge-chunks -o <path to merging result file> -c <path to summarizing chunk file > -r ~/<chunks directory> -i <list of chunk ids> -s <path to system cache directory>
```

Example:

```bash
actev merge-chunks -o ~/output.json -c ~/merging_chunks.json -r ~/chunk_dir/ -i Chunk1 Chunk2 -s ~/system_cache_dir
```
