"""Entry point module: validate-system

This file should not be modified.
"""

from diva_evaluation_cli.bin.private_src.implementation.validate_system.\
    validate_system import validate_system


def entry_point(strict):
    """Private entry point.

    Test the execution of the system on each validation data set provided in
    container_output directory

    Args:
        strict (bool): Whether to cause a failure in case of an error or not

    Checks the structure of the  directory after ActEV-system-setup is run.
    Checks for expected API contents, etc.

    """
    validate_system(strict)
