"""Entry point module: merge-chunks

Implements the entry-point by using Python or any other languages.
"""


def entry_point(system_cache_dir, output_file, chunks_file, chunk_ids,
                proposals=None, localization=None):
    """Method to complete: you have to raise an exception if an error occured
    in the program.

    Given a list of chunk ids, merges all the chunks system output present in
    the list.

    Args:
        system_cache_dir (str): Path to system cache directory
        output_file (str): Path to the output file generated
        chunks_file (str):  Path to generate a chunk file, that summarizes all
            the processed videos/activities
        chunk_ids (:obj:`list`): List of chunk ids
        proposals (str, optional): path to generate a proposals file, that 
            contains the system proposals
        localization (str, optional): path to generate a localization file

    """
    raise NotImplementedError("You should implement the entry_point method.")
